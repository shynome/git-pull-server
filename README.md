### Usage

```sh
docker run -d \
  --name dd \
  --token hello_nodejs \
  -p 8080:80 \
  shynome/git-pull-server
```
```sh
curl -sSL http://127.0.0.1:8080
```