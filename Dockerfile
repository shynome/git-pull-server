FROM node:carbon-alpine

RUN apk add --no-cache \
    openssh git

COPY lib/server.js /git-pull-server.js

STOPSIGNAL SIGTERM

EXPOSE 80

WORKDIR /app

CMD [ "node", "/git-pull-server.js" ]
