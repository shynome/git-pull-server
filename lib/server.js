const token = new RegExp(process.env.token || 'hello_nodejs')
const branch = process.env.branch || 'master'
const remote = process.env.remoteBranch || 'origin'
const git_dir = process.env.git_dir || process.cwd()

console.log(JSON.stringify({
  token:token.source,
  branch,
  remote,
  git_dir,
},null,2))

const Url = require('url')
const { parse: QueryStringParse } = require('querystring')
const { spawn } = require('child_process')

require('http').createServer((req,res)=>{
  const { query: querystring } = Url.parse(req.url)
  if(!token.test(querystring)){
    res.end('token is not right')
    return
  }

  let query = QueryStringParse(querystring)
  
  for(let key in query){
    if(Array.isArray(query[key])){
      res.end(`query params don't support string[]. The err key is ${key}`)
      return
    }
  }
  
  let params = /**@type { { [key:string]: string } }*/({
    'remote': query.remote || remote,
    'git_dir': query.git_dir || git_dir,
    'branch':  query.branch || branch,
  })
  
  let git_pull = spawn('git',['pull',params.remote, params.branch],{ cwd: params.git_dir })

  git_pull.stdout.pipe(res)
  git_pull.stderr.pipe(res)
  git_pull.once('error',err=>{
    res.statusCode = 500
    let info = {
      params,
      err,
    }
    res.end(JSON.stringify(info,null,2))
  })

})
.listen(80,function(){
  // @ts-ignore
  console.log(`git pull server is running at port ${this.address().port}`)
})